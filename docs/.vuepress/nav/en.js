module.exports = [
    {
        text: 'Concepts',
        link: '/concepts/'
    },
    {
        text: 'Support',
        link: 'https://support.salescloud.is'
    },
    {
        text: 'Developers',
        link: 'https://accounts.salescloud.is?platform=developers'
    }
]