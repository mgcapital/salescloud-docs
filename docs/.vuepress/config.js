module.exports = {
    title: 'SalesCloud Docs',
    description: 'Just playing around',
    head: [
        ['link', { rel: 'icon', href: 'https://developers.salescloud.is/favicon.ico' }]
    ],
    themeConfig: {
        repo: 'https://bitbucket.org/salescloudteam/salescloud-docs',
        editLinks: true,
        docsDir: 'docs',
        smoothScroll: true,
        locales: {
            '/': {
                label: 'English',
                selectText: 'Languages',
                ariaLabel: 'Select language',
                editLinkText: 'Edit this page on Bitbucket',
                lastUpdated: 'Last Updated',
                nav: require('./nav/en'),
                sidebar: {
                    '/salescloud-payment-request-api/': [
                        {
                            title: 'Reference',
                            collapsable: false,
                            children: [
                                '',
                            ]
                        },
                    ],
                    '/salescloud-graph/': [
                        {
                            title: 'Graph',
                            collapsable: false,
                            children: [
                                '',
                            ]
                        },
                    ],
                    '/salescloud-apps/': [
                        {
                            title: 'Guide',
                            collapsable: false,
                            children: [
                                'getting-started',
                                'authentication',
                                'lifecycles',
                                'developing',
                                'database',
                                'testing',
                                'releasing',
                                'debugging',
                                'limitations',
                                'examples'
                            ]
                        },
                        {
                            title: 'Events',
                            collapsable: false,
                            children: [
                                'introduction-events',
                                'items',
                                'bookings',
                                'customers',
                                'payments',
                                'dashes',
                                'subscriptions',
                                'delivery-method-instances',
                                'payment-method-instances',
                                'staff',
                                'work',
                                'mappings',
                                'messages',
                                'orders',
                                'invoices',
                                'line-items'
                            ]
                        },
                        {
                            title: 'Definitions',
                            collapsable: false,
                            children: [
                                'availability-methods',
                                'checkout-sections',
                                'checkout-steps',
                                'dash',
                                'delivery-methods',
                                'document-types',
                                'mapping-methods',
                                'payment-methods',
                                'routes',
                                'salescloud-pos-fragments',
                                'item-types',
                                'line-item-types',
                                'css',
                                'js'
                            ]
                        },
                        {
                            title: 'Testing',
                            collapsable: false,
                            children: []
                        },
                        {
                            title: 'Deployment',
                            collapsable: false,
                            children: []
                        }
                    ],
                    '/concepts/': [
                        {
                            title: 'Concepts',
                            collapsable: false,
                            children: [
                                'mappings',
                                'locations',
                                'channels',
                                'organizations',
                                'widgets'
                            ]
                        }
                    ]
                }
            }
        }
    },
    plugins: [
        [
            '@vuepress/google-analytics',
            {
                'ga': 'UA-53948108-8'
            }
        ]
    ]
}

function getGuideSidebar (groupA, groupB) {
    return [
        {
            title: groupA,
            collapsable: false,
            children: [
                '',
                'getting-started',
                'directory-structure',
                'basic-config',
                'assets',
                'markdown',
                'using-vue',
                'i18n',
                'deploy'
            ]
        },
        {
            title: groupB,
            collapsable: false,
            children: [
                'frontmatter',
                'permalinks',
                'markdown-slot',
                'global-computed'
            ]
        }
    ]
}

function getThemeSidebar (groupA, introductionA) {
    return [
        {
            title: groupA,
            collapsable: false,
            sidebarDepth: 2,
            children: [
                ['', introductionA],
                'using-a-theme',
                'writing-a-theme',
                'option-api',
                'default-theme-config',
                'blog-theme',
                'inheritance'
            ]
        }
    ]
}