# Organizations

## Introduction

Organizations represent the actual owners of all objects in the SalesCloud Platform and are not to be confused with users.

An organization as such does not have permissions or authentication.

A user/app however have access to at least a single organization with various permissions on how to access the organizations data.

A single user may have unrestricted access to organization a but more limited access to organization b. A single user can switch between organizations.