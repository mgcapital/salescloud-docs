# Channels

## Introduction

SalesCloud is a sales platform with a focus on selling items. However simple that may sound the truth of the matter is 
that we live in a modern age where sales processes have evolved far from simple in/out transactions. 

People want to sell all sorts of things which we refer to as items but such things can be everything from physical goods 
such as coffee cups to booking a flight ticket. All the intricacies between selling such a variety of things must be
controlled in a clear way.

The process of selling a cup of coffee or selling flight ticket is very different but not in all ways. Both processes share
similarities such as but limited to requesting a payment and accepting terms and agreements. Each process is also based on requesting information
spread out in several steps. We refer to this as checkout steps with varying checkout sections.

A sales channel represents the pipeline if you will. Channels control all the intricacies of which checkout sections should
be displayed on which steps. They control the process of selling items.

A sales channel thereby also represents the origin of a sale. Sales channels are used for online sales processes, point of sale process and kiosk processes alike.

The SalesCloud Platform as a whole works to leverage the above in many ways such as reporting and configuration management.
