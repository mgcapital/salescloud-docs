# Database

Applications get access to two databases in mongodb:
* Application Database
* Organization Database


## Application Database
The Application Database is unique to the install of your application. 

When your application is installed a new application database is created and always passed to your application in context objects.

When your application database is dumped when uninstalling your app.

Please understand that your application does not get a global database to share information across installations. Instead each and every installation creates a new database that is unique for that installation.

Thus uninstalling & reinstalling your application can be part of a troubleshooting mechanism that you can recommend to your users.

Your application has unrestricted access to this database. 
Other applications cannot access this database.

Refer to the mongodb nodejs driver reference for complete examples of how to use the database.

``` js
{
    ...
    onRun: async function(context) {
        // This is an example of you to use the organization database

        const someCustomData = [
            {
                foo: 'bar'
            },
            {
                hello: 'developer'
            }
        ]

        // Inserting data into your app
        await context.appData.collection('myCustomCollection').insertMany(someCustomData).then(result => {
            console.log(result)
        })

        // Querying your apps data
        await context.appData.collection('myCustomCollection').find({}).toArray().then(results => {
            console.table(results)
            // Will output someCustomData with _id's
        })
    },
}
```

## Organization Database

The Organization Database is is unique to the organization that has installed your application. You may want to use this database to query data such as items, categories, customers & more.

This database is accessible to all other installed applications. 

Refer to the mongodb nodejs driver reference for complete examples of how to use the database.

``` js
{
    ...
    onRun: async function(context) {
        // This is an example of you to use the organization database

        // Querying the items (products)
        await context.organizationData.collection('items').find({}).toArray().then(results => {
            console.table(results)
        })

        // Querying the customers
        await context.organizationData.collection('customers').find({}).toArray().then(results => {
            console.table(results)
        })
    },
}
```