# SalesCloud Apps

## How it works

SalesCloud Apps are applications written in javascript to be run in the node runtime.

## Developer portal

SalesCloud Developer portal is where applications are written.

## Appstore

SalesCloud Appstore is where your applications are written.
