# Payment Method Instances

## Created

This event is emitted after a new payment method instance has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('paymentMethodInstanceCreated', function(paymentInstance) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing delivery method instance has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('paymentMethodInstanceUpdated', function(paymentMethodInstance) {
    // Add your logic here.
})

```