# Items

## Created

This event is emitted after a new item has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('newItemCreated', function(item) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing customer has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('itemUpdated', function(item) {
    // Add your logic here.
})

```

## Disabled

This event is emitted after an existing item has been disabled.

An app can listen to the event as shown below.

``` js

context.eventbus.on('itemDisabled', function(item) {
    // Add your logic here.
})

```

## Enabled

This event is emitted after an existing item has been enabled.

An app can listen to the event as shown below.

``` js

context.eventbus.on('itemEnabled', function(item) {
    // Add your logic here.
})

```