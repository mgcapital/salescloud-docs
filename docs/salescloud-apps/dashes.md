# Mappings

## Created

This event is emitted after a new dash has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('dashCreated', function(dash) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing dash has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('dashUpdated', function(dash) {
    // Add your logic here.
})

```