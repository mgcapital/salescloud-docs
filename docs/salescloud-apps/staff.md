# Staff

## Created

This event is emitted after a new staff has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('staffCreated', function(staff) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing customer has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('staffUpdated', function(staff) {
    // Add your logic here.
})

```