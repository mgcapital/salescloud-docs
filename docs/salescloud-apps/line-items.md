# Line Items

## Created

This event is emitted after a new line item has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('lineItemCreated', function(lineItem) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing line item has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('lineItemUpdated', function(lineItem) {
    // Add your logic here.
})

```