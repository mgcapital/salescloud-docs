# Payments

## Created

This event is emitted after a new payment has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('newPaymentCreated', function(payment) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing payment has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('paymentUpdated', function(payment) {
    // Add your logic here.
})

```

## Cancelled

This event is emitted after an existing payment has been cancelled.

An app can listen to the event as shown below.

``` js

context.eventbus.on('paymentCancelled', function(payment) {
    // Add your logic here.
})

```

## Refunded

This event is emitted after an existing payment has been refunded.

An app can listen to the event as shown below.

``` js

context.eventbus.on('paymentRefunded', function(payment) {
    // Add your logic here.
})

```