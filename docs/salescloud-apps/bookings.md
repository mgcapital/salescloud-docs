# Bookings

## Created

This event is emitted after a new booking has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('newBookingCreated', function(booking) {
    // Add your logic here.
})

```

## Redeem

This event is emitted after a new booking has been redeemed.

An app can listen to the event as shown below.

``` js

context.eventbus.on('bookingRedeemed', function(booking) {
    // Add your logic here.
})

```