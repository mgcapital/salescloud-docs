# Getting Started

## Requirements

Creating SalesCloud Applications requires you to have an account to SalesCloud Admin.

Head on over to the signup page to register for free if you have not already.

## Signing into the SalesCloud Developer Portal

Sign in here to the developer portal: https://accounts.salescloud.is?platform=developers