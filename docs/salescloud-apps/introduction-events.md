# Introduction

Events are both emitted by the SalesCloud Platform and SalesCloud Apps and give your apps the ability to react to internal events.

## EventBus

The eventbus is exposed on app context objects passed to your SalesCloud App in lifecycle callbacks. You must emit and declare listeners with the eventbus.

``` js

app.lifeCycle.onStart(appContext => {
    if(appContext.eventbus) {
        appContext.eventbus.on('orderPaidInFull', function(order) {
            // Add your logic here
        }) 
    }
})

```

## Custom Events

Your app can emit custom events that other apps can listen to. This is useful if you write multiple apps and for apps to hook into each other where necessary.

``` js

app.lifeCycle.onStart(appContext => {
    if(appContext.eventbus) {
        appContext.eventbus.emit('myCustomEvent', { foo: 'bar' })
    }
})

app.lifeCycle.onStart(appContext => {
    if(appContext.eventbus) {
        appContext.eventbus.on('myCustomEvent', function(object) {
            // Add your logic here
            console.log(object.foo)
        }) 
    }
})

```

## Keep it for external publish/subscribe

Please don't use the eventbus for publishing and subscribing to the same event in the same app. Doing so will help you preserve the order of execution of your code and prevent unexpected race conditions.