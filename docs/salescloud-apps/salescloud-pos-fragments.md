# SalesCloud POS Fragments

POS Fragments represent a third-party content window that displays in the SalesCloud POS.

This is useful to embed external data into the pos for example to reduce context switching.

## Base definition

``` js
const posFragment = {
    title: "My Custom Third Party Content",
    description: "This is my third party content window",
    namespace: "my_custom_third_party_content",
    callbacks: {
    }
}
```

## Example

``` js
const posFragment = {
    title: "My Custom Third Party Content",
    description: "This is my third party content window",
    namespace: "my_custom_third_party_content",
    configFields: [],
    uiFields: [],
    callbacks: {
        webviewURL: function(context) {
            const organizationUUID = context.organization.uuid
            const serialNumber = context.serialNumber
            return 'https://mydomain.com/?organization=' + organizationUUID + '&serialNumber=' + serialNumber
        }
    }
}
```