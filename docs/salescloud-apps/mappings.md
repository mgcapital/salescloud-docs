# Mappings

## Created

This event is emitted after a new mapping has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('mappingCreated', function(mapping) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing mapping has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('mappingUpdated', function(mapping) {
    // Add your logic here.
})

```