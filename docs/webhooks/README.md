# Webhooks

## Introduction

Introduction
SalesCloud handles both outgoing and incoming webhooks. 

Outgoing & incoming webhooks contain the following headers:

X-SalesCloud-Date
X-SalesCloud-Access-Key
X-SalesCloud-Signature

### X-SalesCloud-Date

The formatted date at which the http request was generated.
Example: 2018-09-10 15:00:22

### X-SalesCloud-Access-Key

The access key as string. Example: 90f298d6c03d011e927e884462f9575305f738b1f876f55b

### X-SalesCloud-Signature

The computed sha256 signature of several values appended as string with a secret key.
The string is appended as follows:   

### Example

``` js
[Sign] = [X-SalesCloud-Date][X-SalesCloud-Access-Key][HTTP_METHOD][RECEIVING_URL][HTTP_BODY]

[Signature] = hash(“sha256”, [Sign], [SecretKey]);

The receiver of the message then uses the locally computed signature to compare the signature supplied in the http header (X-SalesCloud-Signature):

if( [Signature] === [X-SalesCloud-Signature] ) {
// Request is considered valid.
}
Else {
// Request cannot be trusted. Please do no return 200 OK.
}
```
